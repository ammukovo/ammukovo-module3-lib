package ammukovo_module3_lib

import (
	"bufio"
	"os"
)

func CountWords(filepath string) (int64, error) {
	return countByScanner(filepath, bufio.ScanWords)
}

func CountLines(filepath string) (int64, error) {
	return countByScanner(filepath, bufio.ScanLines)
}

func countByScanner(filepath string, split bufio.SplitFunc) (int64, error) {
	file, err := os.Open(filepath)
	if err != nil {
		return 0, err
	}
	defer file.Close()

	scanner := bufio.NewScanner(bufio.NewReader(file))
	scanner.Split(split)

	var counter int64
	for scanner.Scan() {
		counter++
	}
	return counter, nil
}
